SELECT a.nome
FROM aluno a 
WHERE EXISTS (SELECT m.id FROM matricula m WHERE m.aluno_id = a.id);

SELECT a.nome
FROM aluno a 
WHERE NOT EXISTS (SELECT m.id FROM matricula m WHERE m.aluno_id = a.id);

SELECT a.nome
FROM aluno a 
WHERE NOT EXISTS (SELECT m.id FROM matricula m WHERE m.aluno_id = a.id AND m.data < now() - interval 8 month);

SELECT * 
FROM aluno a
LIMIT 0, 3;

SELECT * 
FROM aluno a
WHERE nome LIKE 'João%'
ORDER BY nome
LIMIT 0, 3;

SELECT c.nome, AVG (n.nota)
FROM nota n 
JOIN resposta r on r.id = n.resposta_id
JOIN exercicio e on e.id = r.exercicio_id
JOIN secao s on s.id = e.secao_id
JOIN curso c on c.id = s.curso_id
GROUP BY c.nome

SELECT a.nome, c.nome, AVG(n.nota)
FROM nota n 
JOIN resposta r on r.id = n.resposta_id
JOIN exercicio e on e.id = r.exercicio_id
JOIN secao s on s.id = e.secao_id
JOIN curso c on c.id = s.curso_id
JOIN aluno a on a.id = r.aluno_id
GROUP BY c.nome, a.nome

SELECT a.nome, c.nome, AVG(n.nota)
FROM nota n 
JOIN resposta r on r.id = n.resposta_id
JOIN exercicio e on e.id = r.exercicio_id
JOIN secao s on s.id = e.secao_id
JOIN curso c on c.id = s.curso_id
JOIN aluno a on a.id = r.aluno_id
GROUP BY c.nome, a.nome
HAVING AVG(n.nota) < 5

SELECT DISTINCT tipo 
FROM matricula

SELECT c.nome, COUNT(m.id) 
FROM curso c 
JOIN matricula m ON m.curso_id = c.id 
GROUP BY c.nome

SELECT c.nome, COUNT(m.id) 
FROM curso c 
JOIN matricula m ON m.curso_id = c.id 
WHERE m.tipo IN ('PAGA_PJ', 'PAGA_PF')
GROUP BY c.nome

select a.nome, avg(n1.nota) as media, 
avg(n1.nota) - 
  (
  select avg(n2.nota) from nota n2 
  join resposta r2 on n2.resposta_id = r2.id where r2.aluno_id <> a.id
  ) 
as diferenca
from 
nota n1
join resposta r on r.id = n1.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join aluno a on a.id = r.aluno_id
group by a.nome

select a.nome, r.resposta_dada from 
aluno a left join resposta r on a.id = r.aluno_id
and r.exercicio_id = 1